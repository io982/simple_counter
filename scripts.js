const input_block = document.getElementById('input');
const output_block = document.getElementById('result');
//input_block.onchange = () => {output_block.textContent = "result: " + magic(input_block.value);}
input_block.onchange = () => { output_block.textContent = "result: " + regExMagic(input_block.value); }


let magic = (expresion = "0") => {
    let [i, j] = expresion.split('*');
    let result = j ? i * j : undefined;
    result ?? ([i, j] = expresion.split('+'));
    result ?? (j ? (result = Number(i) + Number(j)) : result = undefined);
    result ?? ([i, j] = expresion.split('/'));
    result ?? (j ? result = i / j : result = undefined);
    result ?? ([i, j] = expresion.split('-'));
    result ?? (j ? result = i - j : result = undefined);
    result ?? ([i, j] = expresion.split('%'));
    result ?? (j ? result = i % j : result = undefined);
    return result ?? "unknown expression";
};

let regExMagic = (expresion = "0") => {
    let regEx = /^(\d+)([\\+\\-\\*\\/\\%])(\d+)/;
    if (regEx.test(expresion)) {
        let resorce = expresion.match(regEx);        
        switch (resorce[2]) {
            case '*':
                return resorce[1] * resorce[3];
            case '+':
                return Number(resorce[1]) + Number(resorce[3]);
            case '/':
                return resorce[1] / resorce[3];
            case '-':
                return resorce[1] - resorce[3];
            default:
                return resorce[1] % resorce[3];
        }
    }
    return "unknown expression";
}

let regExMagic01 = (expresion = "0") => {
    let regEx = /^(\d+)([\\+\\-\\*\\/\\%])(\d+)/;
    if (regEx.test(expresion)) {
        let resorce = expresion.match(regEx);
        //insecure!!!
        return eval(resorce[1] + resorce[2] + resorce[3]);        
    }
    return "unknown expression";
}




console.log('test ' + regExMagic("6%4"));